//
//  BaseVCtrl.swift
//  BaseControl
//
//  Created by Lam Hong Bac on 6/10/19.
//  Copyright © 2019 privacy.chopchep. All rights reserved.
//

import UIKit

open class BaseVCtrl: UIViewController {

    private var vLoading:UIView?
    
    public init() {
        super.init(nibName: String(describing: type(of: self)), bundle: Bundle(for: type(of: self)))
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    open func notification() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc open func keyboardWillShow(notification: NSNotification) -> Void {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc open func keyboardWillHide(notification: NSNotification) -> Void {
        if let gestureRecognizers = view.gestureRecognizers {
            for gr in gestureRecognizers {
                if ((gr as? UITapGestureRecognizer) != nil) {
                    self.view.removeGestureRecognizer(gr)
                }
            }
        }
    }
    
    public func showLoading(_ isShowloading:Bool = true){
        if isShowloading && vLoading == nil {
            if let window = UIApplication.shared.delegate?.window {
                vLoading = UIView(frame: CGRect(x: 0, y: 0, width: window?.frame.width ?? 0, height: window?.frame.height ?? 0))
                vLoading!.backgroundColor = .gray
                vLoading!.alpha = 0.5
                // view into indicator
                let vIndicator = UIView(frame:CGRect(x: 0, y: 0, width: 60, height: 60))
                vIndicator.center = vLoading!.center
                vIndicator.backgroundColor = .black
                vIndicator.alpha = 0.9
                vIndicator.clipsToBounds = true
                vIndicator.layer.cornerRadius = 10
                
                // create indicator
                let indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
                indicator.style = .white
                indicator.center = CGPoint(x: vIndicator.frame.size.width / 2, y: vIndicator.frame.size.height / 2)
                
                vIndicator.addSubview(indicator)
                vLoading!.addSubview(vIndicator)
                window?.addSubview(vLoading!)
                indicator.startAnimating()
            }
        } else {
            if let view = vLoading {
                view.removeFromSuperview()
                vLoading = nil
            }
        }
    }
}
