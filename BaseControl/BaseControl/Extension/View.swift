//
//  View.swift
//  BaseAMZ
//
//  Created by Kim Tai on 3/15/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation

public extension UILabel{
    @IBInspectable var translateKey:String? {
        get {
            return self.translateKey
        }
        set {
            self.text = newValue
        }
    }
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}

public extension UIView {
    func viewShowIndicator(_ isLoading:Bool = true) {
        if isLoading {
            let vLoading = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
            vLoading.backgroundColor = .clear
            vLoading.alpha = 0.5
            vLoading.tag = 100
            // view into indicator
            let vIndicator = UIView(frame:CGRect(x: 0, y: 0, width: 60, height: 60))
            vIndicator.center = vLoading.center
            vIndicator.backgroundColor = .clear
            vIndicator.alpha = 0.9
            vIndicator.clipsToBounds = true
            vIndicator.layer.cornerRadius = 10
            
            // create indicator
            let indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            indicator.style = .gray
            indicator.center = CGPoint(x: vIndicator.frame.size.width / 2, y: vIndicator.frame.size.height / 2)
            
            vIndicator.addSubview(indicator)
            vLoading.addSubview(vIndicator)
            self.addSubview(vLoading)
            indicator.startAnimating()
        }else {
            if let vLoading = self.viewWithTag(100){
                vLoading.removeFromSuperview()
            }
        }
    }
    
    func roundCornersIOS10(_ corners:UIRectCorner,radius:CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func roundCornesIOS11(_ cornes:CACornerMask,radius:CGFloat){
        self.clipsToBounds = true
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = cornes
        }
        self.layer.cornerRadius = radius
    }
    
    
    func shadowView() {
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        self.layer.shadowOpacity = 2.0
        self.layer.shadowRadius = 1.0
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 5
        
    }
}
