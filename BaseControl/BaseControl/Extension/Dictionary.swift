//
//  Dictionary.swift
//  BaseAMZ
//
//  Created by Kim Tai on 3/29/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation

public extension Dictionary{
    public var json:String {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? ""
        } catch {
            return ""
        }
    }
}
