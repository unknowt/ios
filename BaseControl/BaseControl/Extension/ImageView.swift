//
//  ImageView.swift
//  BaseAMZ
//
//  Created by Kim Tai on 4/1/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation
import SDWebImage

public extension UIImage {
    func toBase64String() -> String{
        let data = self.pngData()
        return data?.base64EncodedString(options: .endLineWithLineFeed) ?? ""
        
    }
    
    func resize(newWidth w: CGFloat) -> UIImage? {
        if w >= size.width {
            return self
        }
        
        let scale = w/size.width
        let h = size.height * scale
        
        UIGraphicsBeginImageContext(CGSize(width: w, height: h))
        draw(in: CGRect(x: 0, y: 0, width: w, height: h))
        let newImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImg
    }
    
    func imageWithColor (newColor: UIColor?) -> UIImage? {
        
        if let newColor = newColor {
            UIGraphicsBeginImageContextWithOptions(size, false, scale)
            
            let context = UIGraphicsGetCurrentContext()!
            context.translateBy(x: 0, y: size.height)
            context.scaleBy(x: 1.0, y: -1.0)
            context.setBlendMode(.normal)
            
            let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            context.clip(to: rect, mask: cgImage!)
            
            newColor.setFill()
            context.fill(rect)
            
            let newImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            newImage.accessibilityIdentifier = accessibilityIdentifier
            return newImage
        }
        
        if let accessibilityIdentifier = accessibilityIdentifier {
            return UIImage(named:accessibilityIdentifier)
        }
        
        return self
    }
    
    func maskWithColor(_ color: UIColor) -> UIImage {
        let maskImage = self.cgImage
        let width = self.size.width
        let height = self.size.height
        let bounds = CGRect(x: 0,y: 0,width: width,height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let bitmapContext = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        
        bitmapContext?.clip(to: bounds, mask: maskImage!)
        bitmapContext!.setFillColor(color.cgColor)
        bitmapContext!.fill(bounds)
        
        let cImage = bitmapContext!.makeImage()
        let coloredImage = UIImage(cgImage: cImage!)
        
        return coloredImage
    }
}

public extension UIImageView{
    func setImage(url:URL?,nameImage:String? = "bg_default_tintuc", completion: ((Bool) -> Void)? = nil){
        self.sd_setShowActivityIndicatorView(true)
        self.sd_setIndicatorStyle(.gray)
        self.sd_setImage(with: url, placeholderImage: UIImage(named: nameImage!), options: []) { (image, error, imageCacheType, imageUrl) in
//            self.image = image?.resize(newWidth: self.frame.size.width)
            if completion != nil {
                completion!(true)
            }
        }
    }

}
