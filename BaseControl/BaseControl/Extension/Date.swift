//
//  Date.swift
//  BaseAMZ
//
//  Created by Kim Tai on 2/27/19.
//  Copyright © 2019 Mojave. All rights reserved.
//
import Foundation

public extension Date {
    var dayOfMonth: Int {
        return Calendar.current.component(.day, from: self)
    }
    
    var dayOfWeek: Int {
        return Calendar.current.component(.weekday, from: self)
    }
    
    var monthCurrent: Int {
        return Calendar.current.component(.month, from: self)
    }
    
    var yearCurrent: Int {
        return Calendar.current.component(.year, from: self)
    }
    
    var hour: Int {
        return Calendar.current.component(.hour, from: self)
    }
    
    var minute: Int {
        return Calendar.current.component(.minute, from: self)
    }
    
    var second: Int {
        return Calendar.current.component(.second, from: self)
    }
    
    /// toString with format
    func string(_ stringFormat: String = "dd/MM/yyyy", timeZone: String? = nil, localeID: String = "en") -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: localeID)
        if let timeZone = timeZone {
            if timeZone.lowercased() == "local" {
                formatter.timeZone = TimeZone.current
            } else if timeZone.lowercased() == "utc" {
                formatter.timeZone = TimeZone(abbreviation: "UTC")
            }
        }
        
        formatter.dateFormat = stringFormat
        return formatter.string(from: self)
    }
    
    /// To String with user local format
    func toStringWithLocal(dateFormat: DateFormatter.Style, timeFormat: DateFormatter.Style = .none) -> String {
        return DateFormatter.localizedString(from: self, dateStyle: dateFormat, timeStyle: timeFormat)
    }
    
    /// isMaxDay
    func isMaxDate() -> Bool {
        return yearCurrent >= 9000
    }
    
    /// isMinDay
    func isMinDate() -> Bool {
        return yearCurrent <= 1900
    }
    
    /// To UTC
//    func toUTCDateTime(_ stringFormat: String = "dd/MM/yyyy HH:mm:ss") -> Date? {
//        let formatter = DateFormatter()
//        formatter.timeZone = TimeZone(abbreviation: "UTC")
//        formatter.dateFormat = stringFormat
//        let sstring = formatter.string(from: self)
//        return sstring.toDate(stringFormat)
//    }
    
    func isBetweenTime(fromTime: Date, toTime: Date) -> Bool {
        if fromTime.hour > self.hour || toTime.hour < self.hour {
            return false
        }
        
        if fromTime.hour == self.hour && fromTime.minute > self.minute {
            return false
        }
        
        if toTime.hour == self.hour && toTime.minute < self.minute {
            return false
        }
        
        return true
    }
    
    func isBetweenDate(fromDate: Date, toDate: Date) -> Bool {
        if fromDate.yearCurrent > self.yearCurrent || toDate.yearCurrent < self.yearCurrent {
            return false
        }
        
        if fromDate.yearCurrent == self.yearCurrent && fromDate.monthCurrent > self.monthCurrent {
            return false
        }
        
        if toDate.yearCurrent == self.yearCurrent && toDate.monthCurrent < self.monthCurrent {
            return false
        }
        
        if fromDate.yearCurrent == self.yearCurrent && fromDate.monthCurrent == self.monthCurrent && fromDate.dayOfMonth > self.dayOfMonth {
            return false
        }
        
        if toDate.yearCurrent == self.yearCurrent && toDate.monthCurrent == self.monthCurrent && toDate.dayOfMonth < self.dayOfMonth {
            return false
        }
        
        return true
    }
    
    func yearsFrom(_ date:Date) -> Int{
        
        return Calendar.current.dateComponents([.year], from: date, to: self).year!
    }
    
    func monthsFrom(_ date:Date) -> Int{
        return Calendar.current.dateComponents([.month], from: date, to: self).month!
    }
    
    func weeksFrom(_ date:Date) -> Int{
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear!
    }
    
    func daysFrom(_ date:Date) -> Int{
        
        return Calendar.current.dateComponents([.day], from: date, to: self).day!
    }
    
    func hoursFrom(_ date:Date) -> Int{
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour!
    }
    
    func minutesFrom(_ date:Date) -> Int{
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute!
    }
    
    func secondsFrom(_ date:Date) -> Int{
        return Calendar.current.dateComponents([.second], from: date, to: self).second!
    }
    
    func combineTime(time: Date, timeZone: TimeZone? = TimeZone(abbreviation: "UTC")) -> Date {
        var calendar = Calendar.current
        if let timeZone = timeZone {
            calendar.timeZone = timeZone
        }
        var timeComponents = calendar.dateComponents([.hour, .minute, .timeZone], from: time)
        timeComponents.timeZone = calendar.timeZone
        var dateComponents = calendar.dateComponents([.year, .month, .day], from: self)
        dateComponents.hour = timeComponents.hour
        dateComponents.minute = timeComponents.minute
        dateComponents.second = 0
        dateComponents.timeZone = calendar.timeZone
        
        if let dateFromComponents = calendar.date(from: dateComponents) {
            return dateFromComponents
        }
        
        return self
    }
    
    func toString(withTimeZone: String,format: String = "dd/MM/yyyy",localeID: String = "en") -> String {
        let timeZoneInt = Int(withTimeZone) ?? 0
        let df = DateFormatter()
        df.timeZone = TimeZone(secondsFromGMT: timeZoneInt * 3600)
        df.dateFormat = format
        df.locale = Locale(identifier: localeID)
        return df.string(from: self)
    }
    
    func compareDate(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
    
    func toTime12() -> String {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
    
    func addMinute(_ minuteToAdd: Int) -> Date {
        let secondsInHours: TimeInterval = Double(minuteToAdd) * 60
        let dateWithMinutesAdded: Date = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithMinutesAdded
    }
    
    func addHours(_ hoursToAdd: Int) -> Date {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: Date = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
    
    func addDays(daysToAdd: Int) -> Date {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: Date = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    static var minDate: Date {
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        return calendar.date(era: 1, year: 1800, month: 1, day: 1, hour: 0, minute: 0, second: 0, nanosecond: 0)! as Date
    }
    
    static var maxDate: Date {
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        return calendar.date(era: 1, year: 9999, month: 12, day: 31, hour: 12, minute: 0, second: 0, nanosecond: 0)! as Date
    }
    
    func toLocalTime() -> Date {
        let timeZone = NSTimeZone.local
        let seconds : TimeInterval = Double(timeZone.secondsFromGMT(for:self))
        
        let localDate = Date(timeInterval: seconds, since: self)
        return localDate
    }
    
    func startOfDate() -> Date? {
        var calendar = Calendar.current
        if let timeZone = TimeZone(identifier: "UTC") {
            calendar.timeZone = timeZone
        }
        
        let currentDateComponents = calendar.dateComponents([.year, .month, .day], from: self)
        let startOfDate = calendar.date(from: currentDateComponents)
        return startOfDate
    }
    
    
    func endOfDate() -> Date? {
        var calendar = Calendar.current
        if let timeZone = TimeZone(identifier: "UTC") {
            calendar.timeZone = timeZone
        }
        var comps2 = DateComponents()
        comps2.hour = 23
        comps2.minute = 59
        comps2.second = 59
        return calendar.date(byAdding: comps2, to: self.trimTime() ?? self)
    }
    
    func startOfMonth() -> Date? {
        var calendar = Calendar.current
        if let timeZone = TimeZone(identifier: "UTC") {
            calendar.timeZone = timeZone
        }
        let currentDateComponents = calendar.dateComponents([.year, .month], from: self)
        let startOfMonth = calendar.date(from: currentDateComponents)
        return startOfMonth
    }
    
    
    func endOfMonth() -> Date? {
        var calendar = Calendar.current
        if let timeZone = TimeZone(identifier: "UTC") {
            calendar.timeZone = timeZone
        }
        var comps2 = DateComponents()
        comps2.month = 1
        comps2.day = -1
        comps2.hour = 23
        comps2.minute = 59
        comps2.second = 59
        return calendar.date(byAdding: comps2, to: self)
    }
    
    
    func startOfYear() -> Date? {
        var calendar = Calendar.current
        if let timeZone = TimeZone(identifier: "UTC") {
            calendar.timeZone = timeZone
        }
        let currentDateComponents = calendar.dateComponents([.year], from: self)
        return calendar.date(from: currentDateComponents)
    }
    
    func endOfYear() -> Date? {
        var calendar = Calendar.current
        if let timeZone = TimeZone(identifier: "UTC") {
            calendar.timeZone = timeZone
        }
        var currentDateComponents = DateComponents()
        currentDateComponents.year = self.yearCurrent
        currentDateComponents.month = 12
        currentDateComponents.day = 31
        currentDateComponents.hour = 23
        currentDateComponents.minute = 59
        currentDateComponents.second = 59
        return calendar.date(from: currentDateComponents)
    }
    
    func trimTime() -> Date? {
        var calendar = Calendar.current
        if let timeZone = TimeZone(identifier: "UTC") {
            calendar.timeZone = timeZone
        }
        let currentDateComponents = calendar.dateComponents([.year, .month, .day], from: self)
        return calendar.date(from: currentDateComponents)
    }
    
    
    func dateToSeconds(_ removeSec : Bool = false) -> Int{
        var seconds = 0
        let hoursToSeconds = self.hour * 3600
        let minutesToSeconds = self.minute * 60
        if removeSec == false {
            seconds = self.second
        }
        seconds += hoursToSeconds + minutesToSeconds
        return seconds
    }
    
}
