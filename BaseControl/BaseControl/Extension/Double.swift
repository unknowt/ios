//
//  Double.swift
//  BaseAMZ
//
//  Created by Kim Tai on 4/8/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation

public extension Double {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
