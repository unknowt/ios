//
//  String.swift
//  BaseAMZ
//
//  Created by Kim Tai on 2/22/19.
//  Copyright © 2019 Mojave. All rights reserved.
//

import Foundation

public extension String {
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    func changeColor(_ subString:String,color:UIColor = .blue) -> NSMutableAttributedString{
        let range = (self as NSString).range(of: subString)
        let attribute = NSMutableAttributedString.init(string: self)
        attribute.addAttribute(.foregroundColor, value: color, range: range)
        return attribute
    }
    
    func createBarCode() -> UIImage? {
        let data = self.data(using: .ascii)
        if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            if let outputCIImage = filter.outputImage {
                return UIImage(ciImage: outputCIImage)
            }
        }
        return nil
    }
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    var translate: String {
//        var firstString: String = ""
//        var lastSpecialChar: String = self
        
        // loop in reversed string array
//        for (index, char) in unicodeScalars.reversed().enumerated() {
//            let str = String(char)
//
//            // find first non special character from reversed string
//            if str.isAlphabet || str.isNumber {
//                // string to be translated
//                firstString = String(self[0...count - 1 - index])
//
//                // special character
//                lastSpecialChar = index > 0 ? String(self[count - index...count - 1]) : ""
//                break
//            }
//        }
//
//        return firstString.languageValue + lastSpecialChar.languageValue
        return self
    }
    var firstUppercased: String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    func replace(_ oldString:String,newString:String) -> String{
        return self.replacingOccurrences(of: oldString, with: newString,options: NSString.CompareOptions.literal,range:nil)
    }
    func toDate(_ format:String = "dd/MM/yyyy") -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "vi_VN")
        dateFormatter.calendar = .current
        dateFormatter.timeZone = .current
        return dateFormatter.date(from: self) ?? Date()
    }
    func convertFormatter(_ format:String = "dd/MM/yyyy HH:mm:ss", toFormat:String = "dd/MM/yyyy") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale(identifier: "vi_VN")
        formatter.calendar = .current
        formatter.timeZone = .current
        let yourDate = formatter.date(from: self)
        formatter.dateFormat = toFormat
        return formatter.string(from: yourDate?.toLocalTime() ?? Date().toLocalTime())
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
