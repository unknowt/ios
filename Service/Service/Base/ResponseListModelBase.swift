//
//  ResponseListModelBase.swift
//  Service
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 soriBao. All rights reserved.
//

import Foundation
import ObjectMapper

public class ResponseListModelBase<T: Mappable>: ResponseModelBase<T> {

    public var data: [T]?
    public var page: Int?
    public var totalPages: Int?
    public var total: Int?
    
    public var isFirstPage: Bool {
        get {
            return page == 1
        }
    }
    
    public var hasNext: Bool {
        get {
            guard let page = page else { return true }
            return page < (totalPages ?? 1)
        }
    }
    
    // MARK: - Initialization
    required convenience public init?(map: Map) { self.init() }
    
    // MARK: - Mappable
    override public func mapping(map: Map) {
        super.mapping(map: map)
        
        data       <- map["data"]
        page        <- map["page"]
        totalPages  <- map["total_pages"]
        total       <- map["total"]
    }
}
