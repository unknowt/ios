//
//  ResponseModelBase.swift
//  Service
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 soriBao. All rights reserved.
//

import Foundation
import ObjectMapper

public class ResponseModelBase<T: Mappable>: Mappable {
    
    // MARK: - Properties
    
    public var dataa: T?
    public var result: Int?
    public var message: String?
    public var responseTime: Date?
    
    public var isPlaceholderResponse: Bool {
        get {
            return responseTime == nil
        }
    }
    
    // MARK: - Initialization
    
    required convenience public init?(map: Map) { self.init() }
    
    // MARK: - Mappable
    
    public func mapping(map: Map) {
        dataa      <- map["dataa"]
        result     <- map["result"]
        message    <- map["message"]
        responseTime = Date.init()
    }
}


