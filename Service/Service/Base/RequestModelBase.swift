//
//  RequestModelBase.swift
//  Service
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 soriBao. All rights reserved.
//

import Alamofire

struct RequestHeader {
    
    static let json: [String: String] = [
        "Content-Type": "application/json; charset=utf-8",
        "Accept": "application/json"
    ]
}

public class RequestModelBase {
    
    // MARK: - Properties
    
    var url: String!
    var parameters: [String: Any] = [:]
    
    var httpMethod: HTTPMethod
    let headers: [String: String]
    let shouldRequireAccessToken: Bool
    let shouldCacheResponse: Bool
    
    var encoding: ParameterEncoding {
        get {
            return httpMethod == .get ? URLEncoding.default : JSONEncoding.default
        }
    }
    
    // MARK: - Dealloc
    
    deinit {
        debugPrint("deinit: " + String(describing: self))
    }
    
    // MARK: - Initialization
    
    init(httpMethod: HTTPMethod = .get,
         headers: [String: String] = RequestHeader.json,
         shouldRequireAccessToken: Bool = true,
         shouldCacheResponse: Bool = true) {
        
        self.httpMethod = httpMethod
        self.headers = headers
        self.shouldRequireAccessToken = shouldRequireAccessToken
        self.shouldCacheResponse = shouldCacheResponse
    }
}
