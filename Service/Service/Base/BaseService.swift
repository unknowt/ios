//
//  BaseService.swift
//  Service
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 soriBao. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import RxAlamofire
import ObjectMapper

// Response From Server
typealias ResponeDataValid = [String : Any]

struct APIServiceConfiguration {
    static let timeoutRequest: TimeInterval = 10
    static let timeoutResource: TimeInterval = 10
    static let shareReplayCount: Int = 1
}

public class BaseService {
    
    // MARK: - Properties
    private let _manager: SessionManager
    private let scheduler: ConcurrentDispatchQueueScheduler =
        ConcurrentDispatchQueueScheduler(qos: DispatchQoS(qosClass: DispatchQoS.QoSClass.background, relativePriority: 1))
    
    // MARK: - Dealloc
    deinit {
        print("deinit \(self)")
    }
    
    //MARK: - Init
    init(with manager: SessionManager = SessionManager.default) {
        self._manager = manager
    }
    
    // MARK: - Public Methods
    func request<T: Mappable>(with input: RequestModelBase) -> Observable<T> {
        return sendRequest(with: input)
            .map { data -> T in
                if let json = data as? ResponeDataValid, let item = T(JSON: json){
                    return item
                } else {
                    throw ResponseDataError.invalidResponseData(data: data)
                }
        }
    }
    
    
    
    func requestArray<T: Mappable>(with input: RequestModelBase) -> Observable<[T]> {
        return sendRequest(with: input)
            .map { data -> [T] in
                if let jsonArray = data as? [ResponeDataValid] {
                    let response = Mapper<T>().mapArray(JSONArray: jsonArray)
                    return response
                } else {
                    throw ResponseDataError.invalidResponseData(data: data)
                }
        }
    }
    
    // MARK: - Private Methods
    private func sendRequest(with input: RequestModelBase) -> Observable<Any> {
        return _manager.rx
            .request(input.httpMethod,
                     input.url,
                     parameters: input.parameters,
                     encoding: input.encoding,
                     headers: input.headers)
            .observeOn(scheduler)
            .flatMap { dataRequest -> Observable<DataResponse<Any>> in
                dataRequest
                    .rx.responseJSON()
            }
            .map { dataResponse -> Any in
                return try self.process(with: dataResponse)
        }
    }
    
    private func process(with response: DataResponse<Any>) throws -> Any {
        
        let error: Error
        switch response.result {
        case .success(let value):
            guard let statusCode = response.response?.statusCode else {
                error = ResponseError.noStatusCode
                break
            }
            switch statusCode {
            case 200:
                return value
                
            case ResponseError.notModified.rawValue:
                error = ResponseError.notModified
                
            case ResponseError.badRequest.rawValue:
                error = ResponseError.badRequest
                
            case ResponseError.unauthorized.rawValue:
                error = ResponseError.unauthorized
                
            case ResponseError.accessDenied.rawValue:
                error = ResponseError.accessDenied
                
            case ResponseError.notFound.rawValue:
                error = ResponseError.notFound
                
            case ResponseError.methodNotAllowed.rawValue:
                error = ResponseError.methodNotAllowed
                
            case ResponseError.validate.rawValue:
                error = ResponseError.validate
                
            case ResponseError.serverError.rawValue:
                error = ResponseError.serverError
                
            case ResponseError.badGateway.rawValue:
                error = ResponseError.badGateway
                
            case ResponseError.serviceUnavailable.rawValue:
                error = ResponseError.serviceUnavailable
                
            case ResponseError.gatewayTimeout.rawValue:
                error = ResponseError.gatewayTimeout
                
            case ResponseError.noConnection.rawValue:
                error = ResponseError.noConnection
                
            case ResponseError.timeOutError.rawValue:
                error = ResponseError.timeOutError
                
            default:
                error = ResponseError.noStatusCode
            }
            print(value)
            
        case .failure(let e):
            error = e
        }
        throw error
    }
}

