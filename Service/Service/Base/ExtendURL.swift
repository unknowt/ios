//
//  ExtendURL.swift
//  Service
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 soriBao. All rights reserved.
//

import Foundation

// MARK: - FIX ALL
public struct ExtendURL{
    
    private static let baseURL = "http://e-techpack.com"
    
    // MARK: - User
    public struct User {
        public static let login = baseURL + "/api/login"
    }
}
