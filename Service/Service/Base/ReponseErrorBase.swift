//
//  ReponseErrorBase.swift
//  Service
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 soriBao. All rights reserved.
//

import Foundation
import ObjectMapper

enum ResponseError : Int, Error {
    
    case noStatusCode       = 0
    case notModified        = 304
    case badRequest         = 400
    case unauthorized       = 401
    case accessDenied       = 403
    case notFound           = 404
    case methodNotAllowed   = 405
    case validate           = 422
    case serverError        = 500
    case badGateway         = 502
    case serviceUnavailable = 503
    case gatewayTimeout     = 504
    case noConnection       = -1009
    case timeOutError       = -1001
}

enum ResponseDataError: Error {
    case invalidResponseData(data: Any)
}
