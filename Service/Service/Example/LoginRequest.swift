//
//  LoginRequest.swift
//  Service
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 soriBao. All rights reserved.
//

import Foundation

public class LoginRequest: RequestModelBase {
    public var email: String = ""{
        didSet{
            parameters["email"] = email
        }
    }
    
    public var password: String = ""{
        didSet{
            parameters["password"] = password
        }
    }
    
    public init(){
        super.init()
    }
    
}
