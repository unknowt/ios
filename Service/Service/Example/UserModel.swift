//
//  UserModel.swift
//  Service
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 soriBao. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import RxSwift

public class User: Object, Mappable {
    
    // MARK: - Properties
    @objc dynamic public var id: Int = 0
    @objc dynamic public var firstName: String = ""
    @objc dynamic public var lastName: String = ""
    
    // MARK: - Initialization
    
    required convenience public init?(map: Map) { self.init() }
    
    // MARK: - Public Methods
    
    @discardableResult static public func addOrUpdateUsers(with user: [User]) ->Observable<[User]> {
        return RealmDataBase.shared.addOrUpdateObjects(with: user)
    }
    
    @discardableResult static public func getUsers(with primaryKey: Int) -> Observable<User> {
        //        return RealmDataBase.shared.getObjects(with: primaryKey)
        return RealmDataBase.shared.getObject(with: primaryKey)
    }
    
    @discardableResult static func clearUser(with primaryKey: Int) -> Observable<DatabaseServiceError> {
        return RealmDataBase.shared.deleteObject(with: primaryKey, classType: User.self)
    }
    
    // MARK: - Override Methods
    
    override public static func primaryKey() -> String? {
        return "id"
    }
}

// MARK: - Mappable

extension User {
    
    public func mapping(map: Map) {
        id        <- map["id"]
        firstName <- map["firstname"]
        lastName  <- map["lastname"]
    }
}
