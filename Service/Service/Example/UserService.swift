//
//  UserService.swift
//  Service
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 soriBao. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import RxAlamofire

public class UserService {
    
    // MARK: - Properties
    public static let shared = UserService.init()
    
    private lazy var service: BaseService = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = APIServiceConfiguration.timeoutRequest
        configuration.timeoutIntervalForResource = APIServiceConfiguration.timeoutResource
        let manager = Alamofire.SessionManager(configuration: configuration)
        let service = BaseService.init(with: manager)
        return service
    }()
    
    // MARK: - Dealloc
    deinit {
        debugPrint("deinit \(self)")
    }
    
    // MARK: - Initialization
    
    // MARK: - Public Methods
    
    public func fetchUser(requestModel: LoginRequest) -> Observable<LoginReponse<User>> {
        requestModel.url = ExtendURL.User.login
        requestModel.httpMethod = .post
        
        return service.request(with: requestModel)
            .do(onNext: { response in
                guard let user = response.data else { return }
                //Write to Realm
                User.addOrUpdateUsers(with: user)
            })
            .observeOn(MainScheduler.instance)
            .share(replay: APIServiceConfiguration.shareReplayCount)
    }
}
