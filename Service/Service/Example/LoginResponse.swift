//
//  UserResponseModel.swift
//  Service
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 soriBao. All rights reserved.
//


import ObjectMapper

public class LoginReponse<T: Mappable>: ResponseListModelBase<T> {
    
    // MARK: - Initialization
    required convenience init?(map: Map) { self.init() }
    
    // MARK: - Mappable
    override public func mapping(map: Map) {
        super.mapping(map: map)
    }
}
