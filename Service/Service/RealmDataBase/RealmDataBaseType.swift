//
//  RealmDataBaseType.swift
//  Service
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 soriBao. All rights reserved.
//

import Foundation
import Foundation
import RxSwift
import RealmSwift

typealias DatabaseServicesResults = Results

enum DatabaseServiceError: Error {
    case getObjectFailed(Any)
    case getObjectsFailed
    case addOrUpdateObjectFailed(Any)
    case addOrUpdateObjectsFailed(Any)
    case deleteObjectWithPrimaryKeyFailed(Any)
    case deleteObjectFailed(Any)
}

protocol RealmDataBaseType where Self: RealmDataBase {
    @discardableResult func getObject<T: Object>(with primaryKey: Any) -> Observable<T>
    @discardableResult func getObjects<T: Object>() -> Observable<DatabaseServicesResults<T>>
    @discardableResult func addOrUpdateObject<T: Object>(with object: T, isUpdate: Bool) -> Observable<T>
    @discardableResult func addOrUpdateObjects<T: Object>(with objects: [T], isUpdate: Bool) -> Observable<[T]>
    @discardableResult func deleteObject<T: Object>(with primaryKey: Any, classType: T.Type) -> Observable<DatabaseServiceError>
    @discardableResult func deleteObject<T: Object>(with object: T) -> Observable<DatabaseServiceError>
}
