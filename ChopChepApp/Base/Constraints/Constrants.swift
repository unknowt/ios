//
//  Constrant.swift
//  ChopChepApp
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 privacy.chopchep. All rights reserved.
//

import Foundation

struct Constraints {
    struct UI {
        
        public static let screenBounds = UIScreen.main.bounds
        public static let screenWidth = screenBounds.width
        public static let screenHeight = screenBounds.height
        public static let baseDeviceWidth: CGFloat = 360.0
        public static let displayScale: CGFloat = screenWidth / baseDeviceWidth
        
        public static func getSafeAreaTopPadding() -> CGFloat {
            guard #available(iOS 11.0, *) else { return 0 }
            let topPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
            return topPadding > 20 ? topPadding : 0
        }
        
        public static func getSafeAreaBottomPadding() -> CGFloat {
            guard #available(iOS 11.0, *) else { return 0 }
            let bottomPadding = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
            return bottomPadding
        }
        
        public static func getSafeAreaTotalPadding() -> CGFloat {
            guard #available(iOS 11.0, *) else { return 0 }
            return getSafeAreaTopPadding() + getSafeAreaBottomPadding()
        }
    }
}
