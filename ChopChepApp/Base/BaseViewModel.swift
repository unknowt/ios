//
//  BaseViewModel.swift
//  ChopChepApp
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 privacy.chopchep. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Action


class BaseViewModel: NSObject {
    
    // MARK: - Properties
    var executing: Observable<Bool>!
    
    lazy var apiErrorRelay: BehaviorRelay<String?> = {
        let apiErrorRelay: BehaviorRelay<String?> = BehaviorRelay.init(value: nil)
        return apiErrorRelay
    }()
    
    lazy var databaseErrorRelay: BehaviorRelay<String?> = {
        let databaseErrorRelay: BehaviorRelay<String?> = BehaviorRelay.init(value: nil)
        return databaseErrorRelay
    }()
    
    // MARK: - Dealloc
    
    deinit {
        //        print("deinit " + String(describing: self))
    }
    
    // MARK: - Initialization
    
    override init() {
        super.init()
        
        configureActions()
        configureActionsSuccess()
        configureActionsError()
        configureRelays()
    }
    
    // MARK: - Public Methods
    
    // MARK: - Override Methods
    
    func configureActions() {
        
    }
    
    func configureActionsSuccess() {
        
    }
    
    func configureActionsError() {
        
    }
    
    func configureRelays() {
        
    }
}
