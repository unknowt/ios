//
//  BindType.swift
//  ChopChepApp
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 privacy.chopchep. All rights reserved.
//

import Foundation
import UIKit

protocol BindType where Self: UIResponder {
    
    associatedtype ViewModelType
    var viewModel: ViewModelType! { get set }
    
    func bindViewModel()
    func handlerViewModelAction()
}

extension BindType where Self: MainBaseVCtrl{
    func bindViewModel(to model: Self.ViewModelType){
        viewModel = model
        loadViewIfNeeded()
        bindViewModel()
        handlerViewModelAction()
    }
}

extension BindType where Self: UIView {
    func bindViewModel(to model: Self.ViewModelType){
        viewModel = model
        bindViewModel()
        handlerViewModelAction()
    }
}
