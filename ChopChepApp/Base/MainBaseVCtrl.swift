//
//  BaseVCtrl.swift
//  ChopChepApp
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 privacy.chopchep. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import BaseControl

class MainBaseVCtrl: BaseVCtrl {
    
    // MARK: - Properties
    
    // MARK: Dealloc
    deinit {
        print("deinit " + String(describing: self))
    }
    
    // MARK: - Init
    
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}
