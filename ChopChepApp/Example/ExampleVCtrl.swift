//
//  ExampleVCtrl.swift
//  ChopChepApp
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 privacy.chopchep. All rights reserved.
//

import UIKit
import BaseControl
import RxCocoa
import RxSwift
import SnapKit

class ExampleVCtrl: MainBaseVCtrl, BindType {

    var viewModel: ExampleViewModel!
    public let touchUpRelay: PublishRelay<Void> = PublishRelay()
    
    @IBOutlet weak var labelCreateDataUI: UILabel!
    @IBOutlet weak var labelSuccess: UILabel!
    @IBOutlet weak var labelFail: UILabel!
    @IBOutlet weak var buttonServiceSuccess: UIButton!
    @IBOutlet weak var buttonCreateDataUI: UIButton!
    @IBOutlet weak var buttonServiceFail: UIButton!
    
    override func configUI(){
        super.configUI()
    }
    
    override func configConstraints() {
        super.configConstraints()
        buttonServiceSuccess.snp.makeConstraints { make in
            make.bottom.equalTo(UIScreen.main.bounds.height / 2).offset(-100)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(50 * Constraints.UI.displayScaleHeight)
        }
        buttonServiceFail.snp.makeConstraints { make in
            make.top.equalTo(UIScreen.main.bounds.height / 2)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(50 * Constraints.UI.displayScaleHeight)
        }
        buttonCreateDataUI.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(100)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(50 * Constraints.UI.displayScaleHeight)
        }
        
        labelSuccess.snp.makeConstraints { make in
            make.top.equalTo(buttonServiceSuccess.snp.bottom).offset(12)
            make.left.trailing.equalToSuperview()
        }
        labelFail.snp.makeConstraints { make in
            make.top.equalTo(buttonServiceFail.snp.bottom).offset(12)
            make.left.trailing.equalToSuperview()
        }
        labelCreateDataUI.snp.makeConstraints { make in
            make.top.equalTo(buttonCreateDataUI.snp.bottom).offset(12)
            make.leading.trailing.equalToSuperview()
        }
    }
    
    override func driveUI() {
        super.driveUI()
    }
    
}

// MARK: - Extension BindType
extension ExampleVCtrl{
    func bindViewModel(){
        viewModel.loginRelay.asDriver()
            .drive(onNext: { [unowned self] user in
                self.labelSuccess.text = user?.firstName
            }).disposed(by: rx.disposeBag)
        
        viewModel.apiErrorRelay
            .asDriver()
            .drive(onNext: { [unowned self] error in
                self.labelFail.text = error
                }).disposed(by: rx.disposeBag)
        
        viewModel.createDataUIRelay
            .asDriver()
            .drive(onNext: {[unowned self] user in
            self.labelCreateDataUI.text = user?.firstName
            }).disposed(by: rx.disposeBag)
    }
        
    func handlerViewModelAction(){
        buttonServiceSuccess.rx
            .controlEvent(UIControl.Event.touchUpInside)
            .bind(to: self.viewModel.onPressButtonSuccess.inputs)
            .disposed(by: rx.disposeBag)
        
        buttonServiceFail.rx
            .controlEvent(UIControl.Event.touchUpInside)
            .bind(to: self.viewModel.onPressButtonFail.inputs)
            .disposed(by: rx.disposeBag)
        
        buttonCreateDataUI.rx.tap
            .subscribe(onNext:{ [unowned self] _ in
                self.viewModel.onPressCreateUI()
            } )
        .disposed(by: rx.disposeBag)
        
    }
}
