//
//  ExampleViewModel.swift
//  ChopChepApp
//
//  Created by soriBao on 28/6/2019.
//  Copyright © 2019 privacy.chopchep. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Action
import BaseControl
import NSObject_Rx
import Service

class ExampleViewModel: BaseViewModel {
    
    private(set) var onPressButtonSuccess: Action<Void, LoginReponse<User>>!
    private(set) var onPressButtonFail: Action<Void, LoginReponse<User>>!
    
    let loginRelay: BehaviorRelay<User?> = BehaviorRelay(value: nil)
    let loginFailRelay: BehaviorRelay<User?> = BehaviorRelay(value: nil)
    let createDataUIRelay: BehaviorRelay<User?> = BehaviorRelay(value: nil)

    
    override func configureActions() {
        super.configureActions()
        onPressButtonSuccess = Action{
            UserService.shared.fetchUser(requestModel: self.createLoginRequest())
        }
        
        onPressButtonFail = Action{
            UserService.shared.fetchUser(requestModel: self.createLoginFailRequest())
        }
    }
    
    override func configureActionsSuccess() {
        super.configureActionsSuccess()
        onPressButtonSuccess
            .elements
            .map({$0.data?.first})
            .bind(to: loginRelay)
            .disposed(by: rx.disposeBag)
        
        onPressButtonFail
            .elements
            .do(onNext: {
                if $0.result == 0{
                    self.apiErrorRelay.accept($0.message)
                }
            })
            .filter({return $0.result == 1 ? true : false})
            .map({$0.data?.first})
            .bind(to: loginFailRelay)
            .disposed(by: rx.disposeBag)
        
    }
    
    override func configureActionsError() {
        super.configureActionsError()
        onPressButtonFail
            .errors
            .filter({
                if case .underlyingError(_) = $0 {
                    return true } else { return false } })
            .map({ error -> String? in
                guard case .underlyingError(let error) = error else { return nil }
                return error.localizedDescription
            })
            .bind(to: apiErrorRelay)
            .disposed(by: rx.disposeBag)
    }
    
    override func configureRelays() {
        super.configureRelays()
        executing = Observable.merge(onPressButtonSuccess.executing, onPressButtonFail.executing)
    }
    
    func onPressCreateUI()  {
        let user = User()
        user.firstName = "test"
        createDataUIRelay.accept(user)
    }
    
    // MARK: - Create Request
    private func createLoginRequest() -> LoginRequest{
        let loginRequest = LoginRequest()
        loginRequest.email = "hooa.ng1001@gmail.com"
        loginRequest.password = "123456"
        return loginRequest
    }
    
    private func createLoginFailRequest() -> LoginRequest {
        let loginRequest = LoginRequest()
        loginRequest.email = "hooa.ng1001@gmail.com"
        loginRequest.password = "123458886"
        return loginRequest
    }
}
